package com.androidacademytest

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime


@RequiresApi(Build.VERSION_CODES.O)
fun main(args : Array<String>){
    var orderAnalyzer = OrdersAnalyzer()

    // Order 1
    /*
    {
        orderId: 554,
        creationDate: "2017-03-25T10:35:20", // Saturday
        orderLines: [
        {productId: 9872, name: 'Pencil', quantity: 3, unitPrice: 3.00}
        ]
    }
*/

    var orderNo1Line1 = OrdersAnalyzer.OrderLine(9872, "Pencil", 3, 3.00.toBigDecimal())
    var arrayListOrder1 = arrayListOf(orderNo1Line1)
    var firstOrder = OrdersAnalyzer.Order(554, LocalDateTime.parse("2017-03-25T10:35:20"), arrayListOrder1)


    // Order 2
    /*
     {
        orderId: 555,
        creationDate: "2017-03-25T11:24:20", // Saturday
        orderLines: [
            {productId: 9872, name: 'Pencil', quantity: 2, unitPrice: 3.00},
            {productId: 1746, name: 'Eraser', quantity: 1, unitPrice: 1.00}
        ]
    },

     */

    var orderNo2Line1 = OrdersAnalyzer.OrderLine(9872, "Pencil", 2, 3.00.toBigDecimal())
    var orderNo2Line2 = OrdersAnalyzer.OrderLine(1746, "Erase", 1, 1.00.toBigDecimal())
    var arrayListOrder2  = arrayListOf(orderNo2Line1, orderNo2Line2)
    var secondOrder = OrdersAnalyzer.Order(555, LocalDateTime.parse("2017-03-25T11:24:20"), arrayListOrder2)


   //Order 3
    /*    {
        orderId: 453,
        creationDate: "2017-03-27T14:53:12", // Monday
        orderLines: [
            {productId: 5723, name: 'Pen', quantity: 4, unitPrice: 4.22},
            {productId: 9872, name: 'Pencil', quantity: 3, unitPrice: 3.12},
            {productId: 3433, name: 'Erasers Set', quantity: 1, unitPrice: 6.15}
        ]
    },
*/
    var orderNo3Line1 = OrdersAnalyzer.OrderLine(5723, "Pen", 4, 4.22.toBigDecimal())
    var orderNo3Line2 = OrdersAnalyzer.OrderLine(9872, "Pencil", 3, 3.12.toBigDecimal())
    var orderNo3Line3 = OrdersAnalyzer.OrderLine(3433, "Erasers Set", 1, 6.15.toBigDecimal())
    var arrayListOrder3 = arrayListOf(orderNo3Line1, orderNo3Line2, orderNo3Line3)
    var thirdOrder = OrdersAnalyzer.Order(453, LocalDateTime.parse("2017-03-27T14:53:12"), arrayListOrder3)

 //Order 4
    /*
        {
        orderId: 431,
        creationDate: "2017-03-20T12:15:02", // Monday
        orderLines: [
            {productId: 5723, name: 'Pen', quantity: 7, unitPrice: 4.22},
            {productId: 3433, name: 'Erasers Set', quantity: 2, unitPrice: 6.15}
        ]
    },
     */

    var orderNo4Line1 = OrdersAnalyzer.OrderLine(5723, "Pen", 7, 4.22.toBigDecimal())
    var orderNo4Line2 = OrdersAnalyzer.OrderLine(3433, "Erasers Set", 2, 6.15.toBigDecimal())
    var arrayListOrder4 = arrayListOf(orderNo4Line1, orderNo4Line2)
    var fourthOrder = OrdersAnalyzer.Order(431, LocalDateTime.parse("2017-03-20T12:15:02"), arrayListOrder4)

 //Order 5
    /*
       {
        orderId: 690,
        creationDate: "2017-03-26T11:14:00", // Sunday
        orderLines: [
            {productId: 9872, name: 'Pencil', quantity: 4, unitPrice: 3.12},
            {productId: 4098, name: 'Marker', quantity: 5, unitPrice: 4.50}
        ]
    }
     */

    var orderNo5Line1 = OrdersAnalyzer.OrderLine(9872, "Pencil", 4, 3.12.toBigDecimal())
    var orderNo5Line2 = OrdersAnalyzer.OrderLine(4098, "Marker", 5, 4.50.toBigDecimal())
    var arrayListOrder5 = arrayListOf(orderNo5Line1, orderNo5Line2)
    var fifthOrder = OrdersAnalyzer.Order(690, LocalDateTime.parse("2017-03-26T11:14:00"),arrayListOrder5)

    var arrayListOrders = arrayListOf(firstOrder,secondOrder,thirdOrder,fourthOrder,fifthOrder)


    for (x in orderAnalyzer.totalDailySales(arrayListOrders)){
        if (x.value != 0){
            println("\"DayOfWeek.${x.key}\" : ${x.value}")
        }
    }
}