package com.androidacademytest


import android.os.Build
import androidx.annotation.RequiresApi
import java.math.BigDecimal
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.HashMap


class OrdersAnalyzer{

    data class Order(val OrderId: Int, val creationDate: LocalDateTime, val orderLines: List<OrderLine>)
    data class OrderLine(val productId: Int, val  name: String, val quantity: Int, val unitPrice: BigDecimal)


    @RequiresApi(Build.VERSION_CODES.O)
    fun stringToDate(date: String): LocalDateTime {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
        return LocalDateTime.parse(date, formatter)
    }

    private fun sumOrdersPrice(orderLines: List<OrdersAnalyzer.OrderLine>): Int {
        var sum = BigDecimal(0)
        for (line in orderLines) {
            sum += (line.unitPrice * line.quantity.toBigDecimal())
        }
        return sum.toInt()
    }

    private fun sumOrdersQuantity(orderLines: List<OrdersAnalyzer.OrderLine>): Int {
        var sum = 0
        for (line in orderLines) {
            sum += line.quantity
        }
        return sum
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun totalDailySales(orders: List<OrdersAnalyzer.Order>): MutableMap<DayOfWeek, Int> {
        val ordersMap = HashMap<DayOfWeek, Int>()
    for (order in orders) {
        val orderDay = order.creationDate.dayOfWeek
        val orderSum = sumOrdersQuantity(order.orderLines)
        ordersMap[orderDay] = ordersMap.getOrDefault(orderDay, 0) + orderSum
    }
    return ordersMap.toSortedMap()
    }
}